
Drupal.behaviors.pluploadfield = function (context) {
	
	$.each(Drupal.settings.pluploadfield.fields, function(index, value) {
		// Convert divs to queue widgets when the DOM is ready

		// Setup uploader
		var uploader = $("#"+value.uploaderid, context).pluploadQueue({
			// General settings
			runtimes : 'html5,flash,html4',
			url : value.url,
			max_file_size : '10mb',
			chunk_size : '1mb',
			unique_names : false,
			flash_swf_url : value.swfurl,
		
			// Specify what files to browse for
			filters : [
			   {title : "Image files", extensions : value.extensions}
			]
		});
		
		uploader = $("#"+value.uploaderid, context).pluploadQueue();
		if (uploader != undefined) {
			uploader.bind('FileUploaded', function(up, file, response) {
					var obj = Drupal.parseJson(response.response);
		
					//var deltaorder = $(".draggable .delta-order")[0].innerHTML;
					var deltaorder = Drupal.settings.pluploadfield.deltaorder;
					var re = new RegExp('edit-'+value.form_fieldname+'.(\\d+)-ahah-wrapper',"g");
					var matchArray = re.exec(obj.data);
					if (matchArray) {
						var currentdelta = matchArray[1];
					} else {
						var currentdelta = 0;
					}
					var oddeven = currentdelta%2 == 0 ? 'odd' : 'even';
					deltaorder = deltaorder.replace(value.form_fieldname+"-0", value.form_fieldname+"-"+currentdelta);
					deltaorder = deltaorder.replace(value.fieldname+"[0]", value.fieldname+"["+currentdelta+"]");
		
					$("table#"+value.fieldname+"_values").removeClass("tabledrag-processed");
					$("a.tabledrag-handle", $("table#"+value.fieldname+"_values")).remove();

					// if message error in field
					var data_obj = $(obj.data);
                    if ( data_obj.hasClass('messages') ) {
                        var msg = data_obj.filter('.messages.status');
                        data_obj.remove('.messages.status');
                        $("#content-messages-inner").append(msg);
                    }

					if ( data_obj.hasClass('messages') && data_obj.hasClass('error') ) {
						var msg = data_obj.filter('.messages.error').eq(0);
                        data_obj.remove('.messages.error');
						up.trigger('Error', {
							code : plupload.IMAGE_DIMENSIONS_ERROR,
							message : msg.text(),
							file : file
							});
					} else {
						$("#"+value.form_fieldname+"-items tbody").append('<tr class="draggable '+oddeven+'"><td class="content-multiple-drag"></td><td><div>'+data_obj.html()+'</td><td style="display: none;" class="delta-order">'+deltaorder+'</td></tr>');
						Drupal.attachBehaviors($("#"+value.form_fieldname+"-items"));
					}
					
				});
			uploader.bind('Error',function(upr, error){
		    	$("#content-messages-inner").append('<div class="messages error">'+error.message+'</div>');
		   	});
			
			uploader.bind('QueueChanged', function(up) {
				up.start();
			});	
			
			$("#"+value.form_fieldname+"-items tbody .draggable input.form-file").parents('.draggable').remove();
		}
		
		// save deltaorder for the future (we will delete all)
		if (Drupal.settings.pluploadfield.deltaorder == undefined )
			Drupal.settings.pluploadfield.deltaorder = $(".draggable .delta-order")[0].innerHTML;
		
	});
};
