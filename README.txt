-- SUMMARY --

The Plupload Field module provides a browser-based widget to allow users with the appropriate permissions to upload multiple images in imagefield.

Plupload is a GPL licensed multiple file uploading tool that can present widgets in Flash, Gears, HTML 5, Silverlight, BrowserPlus, and HTML4 depending on the capabilities of the client computer.

-- INSTALLATION --

1. Extract the module into sites/all/modules/
2. Dowload the Plupload library from http://www.plupload.com/
3. Extract the libary into sites/all/libraries/plupload/ (it will look like sites/all/libraries/plupload)
4. Assign user permissions appropriately  /admin/user/permissions
5. Add a CCK imageField
6. Wait, the Plupload widget is working its magic.


