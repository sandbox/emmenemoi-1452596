<?php

/**
 * Admin form.
 */
function pluploadfield_admin_settings() {
  // Figure out which content types have a filefield.
  $types = content_types();
  foreach ($types as $key => $type) {
    foreach ($type['fields'] as $field) {
      if ($field['type'] == 'filefield') {
        $available['matches'][$key .':::'. $field['field_name']] = $type['name'] .': '. $field['widget']['label'];
        $available['keys'][] = $key .':::'. $field['field_name'];
      }
    }
  }

  // Show them a filefield-content-type to use or nag them to create one.
  if (!empty($available['matches'])) {
    $form['pluploadfield_import_field_type'] = array(
      '#type' => 'select',
      '#title' => t('Target field'),
      '#description' => t('Select the specific filefield you want to import photos into. During import, nodes of your selected type will be created and the selected field will be populated with the imported image. The image name will be used for the title and all other fields will be blank.'),
      '#options' => $available['matches'],
      '#default_value' => variable_get('pluploadfield_import_field_type', $available['keys'][0]),
    );
  }
  else {
    $form['error_state'] = array(
      '#value' => t('You must enable a filefield on a content type.'),
    );
  }

  return system_settings_form($form);
}


function pluploadfield_onform($form_build_id, $type_name, $field_name) {
  $temp_directory = file_directory_temp();

  $GLOBALS['devel_shutdown'] = FALSE;

  // Chunk it?
  $chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
  $chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;

  // Get and clean the filename.
  $file_name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

  // Clean the fileName for security reasons
  $file_name = preg_replace('/[^\w\._]+/', '', $file_name);


  // Look for the content type header
  if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
    $content_type = $_SERVER["HTTP_CONTENT_TYPE"];
  }
  if (isset($_SERVER["CONTENT_TYPE"])) {
    $content_type = $_SERVER["CONTENT_TYPE"];
  }

  // Is this a multipart upload?
  if (strpos($content_type, "multipart") !== FALSE) {
    if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
      // Open temp file
      $out = fopen($temp_directory . DIRECTORY_SEPARATOR . $file_name, $chunk == 0 ? "wb" : "ab");
      if ($out) {
        // Read binary input stream and append it to temp file
        $in = fopen($_FILES['file']['tmp_name'], "rb");

        if ($in) {
          while ($buff = fread($in, 4096))
            fwrite($out, $buff);
        }
        else
          die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

        fclose($in);
        fclose($out);
        unlink($_FILES['file']['tmp_name']);
      }
      else
        die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
    }
    else
      die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
  }
  else {
    // Open temp file
    $out = fopen($temp_directory . DIRECTORY_SEPARATOR . $file_name, $chunk == 0 ? "wb" : "ab");
    if ($out) {
      // Read binary input stream and append it to temp file
      $in = fopen("php://input", "rb");

      if ($in) {
        while ($buff = fread($in, 4096))
          fwrite($out, $buff);
      }
      else
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

      fclose($in);
      fclose($out);
    }
    else
      die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
  }

  if ($chunks > 1 && $chunk < $chunks - 1) {
    // Don't move the file and add the node yet, we have more chunks coming
    die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
  }

//filefield/ahah/'.   $element['#type_name'] .'/'. $element['#field_name'] .'/'. $element['#delta']

	$form_state = array('submitted' => FALSE);
  $form = form_get_cache($form_build_id, $form_state);
	$delta = -1;
	foreach ($form[$field_name] as $key=>$value) {
		if (is_array($value) && $value['#default_value']['fid'] > 0)
			$delta = $value['#delta'];
	}

	$delta++;
	$_POST['form_build_id'] = $form_build_id;
	$_POST = $_GET;
  $form['#post'] = $_POST;
	//upload_node_form_submit($form, $form_state);
	$file = pluploadfield_field_save_upload($temp_directory . DIRECTORY_SEPARATOR . $file_name, $file_name, $type_name, $field_name);
   
	$field_type = $form['#field_info'][$field_name]['widget']['type'];
/*
	$form[$field_name][$delta] = array('#field_name'=>$field_name, '#type_name' =>$type_name);
	if ((!empty($field_type)) && ($info = _element_info($field_type))) {
      $form[$field_name][$delta] = $info;
   }
*/
	$form[$field_name][$delta] = $form[$field_name][0];
	$form[$field_name][$delta]['#default_value'] = $file;
	$form[$field_name][$delta]['#delta'] = $delta;
	$form[$field_name][$delta]['#weight'] = $delta;
	$form[$field_name][$delta]['#access'] = 1;

	form_set_cache($form_build_id, $form, $form_state);
	watchdog('pluploadfield', 'info '.$form_build_id.':'.print_r($form[$field_name][$delta], true), array(), WATCHDOG_NOTICE);
	filefield_js($type_name, $field_name, $delta);

  // @todo check the $image_node and do some error handling.

  // Return JSON-RPC response
  die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
}


function pluploadfield_field_save_upload($temp_filepath, $file_name, $type, $field_name) {
  // Only get files from Drupal's tmp directory.
  $directory = file_directory_temp();
  if (file_check_location($temp_filepath, $directory)) {
    // Only get files where we can get some image info.
    if ($info = image_get_info($temp_filepath)) {
      
      // Get the field and its validators.
      $field = content_fields($field_name, $type);
      $validators = pluploadfield_widget_upload_validators($field);

      // make sure that the directory exists
      $directory = filefield_widget_file_path($field);
      field_file_check_directory($directory, FILE_CREATE_DIRECTORY);

      // Create some defaults that imagefield expects.
      $form_state_values = array(
        'title' => $file_name,
        'body' => '',
        'field_photo' => array(0 => array(
            'fid' => 0,
            'list' => '1',
            'filepath' => '',
            'filename' => '',
            'filemime' => '',
            'filesize' => 0,
            'filefield_upload' => 'Upload',
            'filefield_remove' => 'Remove',
            'upload' => 0,
          ),
        'node_status' => NULL,
        )
      );

      // Save the file and create a node.
      if ($file = field_file_save_file($temp_filepath, $validators, $directory)) {
        //$file['original_path'] = $temp_filepath;

        // Add the description if the settings call for it (it can be turned off).
        if ($field['description_field'] === '1') {
          $file['data']['description'] = '';
        }

        // Add the default alt and title text if configured to have defaults (they're always 'on').
        $file['data']['alt'] = isset($field['widget']['alt']) ? $field['widget']['alt']: '';
        $file['data']['title'] = isset($field['widget']['title']) ? $field['widget']['title']: '';

        // Set list status to default.
        $file['list'] = $field['list_field'] === '1' && $field['list_default'] === 0 ? '0' : '1';

        //$node = _pluploadfield_imagefield_import_create_node($field, $form_state_values, $file, $options);
        //file_delete($temp_filepath);
      }

    }
  }
  return $file;
}

function pluploadfield_replace_add(&$form, &$form_state, $field) {
    global $language;
	$mod_path = drupal_get_path('module', 'pluploadfield');
    $path = libraries_get_path('plupload');
	drupal_add_js($mod_path . '/pluploadfield.js');
	drupal_add_js($path . '/js/plupload.full.js');
    if ($language->language != 'en')
        drupal_add_js($path . '/js/i18n/'.$language->language.'.js');
    drupal_add_js($path . '/js/jquery.plupload.queue/jquery.plupload.queue.js');
    drupal_add_css($path .'/js/jquery.plupload.queue/css/jquery.plupload.queue.css');
	/*drupal_add_js($path . '/plupload/js/plupload.html5.js');
	
	drupal_add_js($path . '/plupload/js/plupload.flash.js');
	*/

	// Get the field and its validators so we can build our extension list.
	
	/*
	$field = content_fields($field_name, $type);
	$validators = imagefield_widget_upload_validators($field);
	$extensions = str_replace(' ', ',', $validators['filefield_validate_extensions'][0]);
	*/

/*
UPLOAD_IDENTIFIER	ed1ab6d00904af0767e53b49e76f4655
field_cover_images[0][fid]		0
field_cover_images[0][list]	1
field_cover_images[2][_weight]	2
field_cover_images[3][UPLOAD_IDENTIFIER]	51ff5a4c95867ac0db2ef9c42313a9fc
field_cover_images[3][fid]		0
field_cover_images[3][list]	1
field_cover_images[3][_weight]	3
teaser_include	1
body	
format	1
field_price[0][value]	
language	en
changed	
form_build_id	form-dcddba7f031ec1023059d110a34ed84e
form_token	c0514bdacc904335444edffaa1bb9803
form_id	standard_product_node_form
menu[link_title]	
menu[parent]	primary-links:0
menu[weight]	0
nodewords[abstract][value...	
nodewords[canonical][valu...	
nodewords[copyright][valu...	
nodewords[description][va...	
nodewords[keywords][value...	
nodewords[revisit-after][...	1
nodewords[dc.contributor]...	
nodewords[dc.creator][val...	
nodewords[dc.date][value]...	24
nodewords[dc.date][value]...	1
nodewords[dc.date][value]...	2011
nodewords[dc.title][value...	
nodewords[location][latit...	
nodewords[location][longi...	
nodewords[pics-label][val...	
log	
comment	0
pathauto_perform_alias	1
name	webmaster
date	
status	1
op	Upload
*/

	$query_string = array(
    'op-desactivated' => 'Upload',
		'form_build_id'   => $form['#build_id'],
		'form_id'         => $form['form_id']['#value'],
    'form_token'      => $form['form_token']['#default_value']
	);

	$url = url('pluploadfield/form/'.$form['#build_id'].'/'.$field['type_name'].'/'.$field['field_name'], array('query' => $query_string));
	$swfurl = url($path .'/js/plupload.flash.swf');
	$uploader_id = "uploader-".$field['field_name']."-".$form['#build_id'];
	$extensions = str_replace(' ', ',',$form['#field_info'][$field['field_name']]['widget']['file_extensions']);
	$form_fieldname = str_replace("_", "-", $field['field_name']);
	$fieldname = $field['field_name'];
	
	// get current module settings
	$javascript = drupal_add_js(NULL, NULL, 'header');
	$setting['pluploadfield'] = !empty($javascript['setting']['pluploadfield']) ? $javascript['setting']['pluploadfield'] : array();
	$setting['pluploadfield']['fields'][] = array(
	      'uploaderid'     =>$uploader_id,
	      'url'            =>$url,
	      'swfurl'         =>$swfurl,
	      'extensions'     =>$exensions,
	      'form_fieldname' =>$form_fieldname,
	      'fieldname'      =>$fieldname
	      );
	
	drupal_add_js($setting, 'setting', 'header');
	/*
	$script = "
	// Convert divs to queue widgets when the DOM is ready
	$(function() {
	// Setup uploader
	$(\"#$uploader_id\").pluploadQueue({
	// General settings
	runtimes : 'html5,flash,html4',
	url : '$url',
	max_file_size : '10mb',
	chunk_size : '1mb',
	unique_names : false,
	flash_swf_url : '$swfurl',

	// Specify what files to browse for
	filters : [
	   {title : \"Image files\", extensions : \"$extensions\"}
	]
	});
	
	var uploader = $(\"#$uploader_id\").pluploadQueue();
	uploader.bind('FileUploaded', function(up, file, response) {
			var obj = Drupal.parseJson(response.response);

			var deltaorder = $(\".draggable .delta-order\")[0].innerHTML;
			var re = /edit-$form_fieldname.(\d+)-ahah-wrapper/g;
			var matchArray = re.exec(obj.data);
			if (matchArray) {
				var currentdelta = matchArray[1];
			} else {
				var currentdelta = 0;
			}
			var oddeven = currentdelta%2 == 0 ? 'odd' : 'even';
			deltaorder = deltaorder.replace(\"$form_fieldname-0\", \"$form_fieldname-\"+currentdelta);
			deltaorder = deltaorder.replace(\"".$fieldname."[0]\", \"".$fieldname."[\"+currentdelta+\"]\");

			$(\"table#".$fieldname."_values\").removeClass(\"tabledrag-processed\");
			$(\"a.tabledrag-handle\", $(\"table#".$fieldname."_values\")).remove();
			if (firstRow != undefined && currentdelta == 0) {
                $(\"#$form_fieldname-items tbody .draggable\").remove();
			}
			$(\"#$form_fieldname-items tbody\").append('<tr class=\"draggable '+oddeven+'\"><td class=\"content-multiple-drag\"></td><td>'+obj.data+'</td><td style=\"display: none;\" class=\"delta-order\">'+deltaorder+'</td></tr>');
			Drupal.attachBehaviors($(\"#$form_fieldname-items tbody\"));
		
		});
	uploader.bind('Error',function(upr, error){
                console.log(error); 
   	});
    
	var firstRow = $(\"#$form_fieldname-items tbody .draggable input.form-file\")[0];
	if ( firstRow != undefined) {
	   $(\"#$form_fieldname-items tbody .draggable\").hide();
	}

	$(\"#edit-".$form_fieldname."-0-filefield-remove\").click(function() { 
	   alert('click'); 
    });
	
	});
	";

	drupal_add_js($script, 'inline');*/

	$output = "<div id=\"$uploader_id\">Your browser does not support HTML5 native or flash upload. Try Firefox 3, Safari 4, or Chrome; or install Flash.</div>";
	//$form[$field['field_name']][$field['field_name'].'_add_more'] = array('#value'=>$output);
	$form[$field['field_name']][$field['field_name'].'_add_more']['#access'] = 0;
	//$form[$field['field_name']]['#prefix'] = $output;
	//$form[$field['field_name']][$field['field_name'].'_add_more']['#weight'] = -10;
	foreach($form[$field['field_name']] as $delta => $value) {
		if (is_array($value) && is_array($value['#default_value']) && !isset($value['#default_value']['filename'])) {
			if ($delta > 0) 
				unset($form[$field['field_name']][$delta]);
		}
	}
	
    $output .= ' <div class="'.str_replace('_','-',$field['field_name']).'-description">' . $form[$field['field_name']]['#description'] . "</div>\n";
    unset($form[$field['field_name']]['#description']);
	$form[$field['field_name']]['#prefix'] .= '<div class="pluploadfield_widget">';
	$form[$field['field_name']]['#suffix'] .= '</div>'.$output;
   
}

function pluploadfield_element_theme(&$element, &$form, $field) {
  
   $query_string = array(
      'op-desactivated' => 'Upload',
      'form_build_id'   => $form['#build_id'],
      'form_id'         => $form['form_id']['#value'],
      'form_token'      => $form['form_token']['#default_value']
   );
   $url = url('pluploadfield/form/'.$form['#build_id'].'/'.$field['type_name'].'/'.$field['field_name'], array('query' => $query_string));
   $swfurl = url($path .'/js/plupload.flash.swf');
   $uploader_id = "uploader-".$field['field_name']."-".$form['#build_id'];
   $extensions = str_replace(' ', ',',$form['#field_info'][$field['field_name']]['widget']['file_extensions']);
   $form_fieldname = str_replace("_", "-", $field['field_name']);
   $fieldname = $field['field_name'];

   // get current module settings
   $javascript = $form['#pluploadfield_settings'];
   $setting = !empty($form['#pluploadfield_settings']) ? $form['#pluploadfield_settings'] : array();
   $setting['fields'][] = array(
         'uploaderid'     =>$uploader_id,
         'url'            =>$url,
         'swfurl'         =>$swfurl,
         'extensions'     =>$extensions,
         'form_fieldname' =>$form_fieldname,
         'fieldname'      =>$fieldname
   );
   $form['#pluploadfield_settings'] = $setting;

   $output = "<div id=\"$uploader_id\">Your browser does not support HTML5 native or flash upload. Try Firefox 3, Safari 4, or Chrome; or install Flash.</div>";
   //$form[$field['field_name']][$field['field_name'].'_add_more'] = array('#value'=>$output);
   $element[$field['field_name'].'_add_more']['#access'] = 0;
   //$form[$field['field_name']]['#prefix'] = $output;
   //$form[$field['field_name']][$field['field_name'].'_add_more']['#weight'] = -10;
   foreach($element as $delta => $value) {
      if (is_array($value) && is_array($value['#default_value']) && !isset($value['#default_value']['filename'])) {
         if ($delta > 0)
            unset($element[$delta]);
      }
   }

   $output .= ' <div class="'.str_replace('_','-',$field['field_name']).'-description">' . $element['#description'] . "</div>\n";
   unset($element['#description']);
   $element['#prefix'] .= '<div class="pluploadfield_widget">';
   $element['#suffix'] .= '</div>'.$output;
    
}

