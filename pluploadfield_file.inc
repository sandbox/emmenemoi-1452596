<?php
/**
 * @file
 * hook_file and imagefield file functions.
 */

/**
 * Implementation of hook_file_insert().
 */
/*
function pluploadfield_file_insert($file) {
  // Currently empty. Thumbnails are now generated on preview.
}
*/
/**
 * Implementation of hook_file_delete().
 *
 * Delete the admin thumbnail when the original is deleted.
 */
/*
function pluploadfield_file_delete($file) {
  imagefield_file_delete($file);
}
*/
/**
 * Implementation of hook_file_references().
 */
/*
function pluploadfield_file_references($file) {

  return imagefield_file_references($file);
}
*/
/**
 * Simple utility function to check if a file is an image.
 */

function pluploadfield_file_is_image($file) {
  $file = (object)$file;
  return in_array($file->filemime, array('image/jpg', 'image/pjpeg', 'image/jpeg', 'image/png', 'image/gif'));
}

/**
 * Given a file, return the path the image thumbnail used while editing.
 */
function pluploadfield_file_admin_thumb_path($file, $create_thumb = TRUE) {

  return imagefield_file_admin_thumb_path($file, $create_thumb);
}

/**
 * Create a thumbnail to be shown while editing an image.
 */
function pluploadfield_create_admin_thumb($source, $destination) {
 imagefield_create_admin_thumb($source, $destination);
}
