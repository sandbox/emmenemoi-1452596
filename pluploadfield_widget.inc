<?php
/**
 * @file
 * pluploadfield widget hooks and callbacks.
 */

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function pluploadfield_widget_settings_form($widget) {
  $form = module_invoke('imagefield', 'widget_settings', 'form', $widget);
  return $form;
}

/**
 * Element specific validation for imagefield default value.
 *
 * Validated in a separate function from imagefield_field() to get access
 * to the $form_state variable.
 */
/*
function _pluploadfield_widget_settings_default_validate($element, &$form_state) {
  // Skip this validation if there isn't a default file uploaded at all or if
  // validation has already failed because of another error.
  if (!is_uploaded_file($_FILES['files']['tmp_name']['default_image_upload']) || form_set_error()) {
    return;
  }

  // Verify the destination exists.
  $destination = file_directory_path() .'/pluploadfield_default_images';
  if (!field_file_check_directory($destination, FILE_CREATE_DIRECTORY)) {
    form_set_error('default_image', t('The default image could not be uploaded. The destination %destination does not exist or is not writable by the server.', array('%destination' => dirname($destination))));
    return;
  }
// $Id: imagefield_formatter.inc,v 1.12 2009/03/21 03:26:57 quicksketch Exp $

  $validators = array(
    'file_validate_is_image' => array(),
  );

  // We save the upload here because we can't know the correct path until the file is saved.
  if (!$file = file_save_upload('default_image_upload', $validators, $destination)) {
    // No upload to save we hope... or file_save_upload() reported an error on its own.
    return;
  }

  // Remove the old default image if any.
  $old_default = $form_state['values']['default_image'];
  if (!empty($old_default['fid'])) {
    // Set a flag to not count this instance in hook_file_references().
    $old_default['pluploadfield_type_name'] = $form_state['values']['type_name'];
    field_file_delete($old_default);
  }

  // Make the file permanent and store it in the form.
  file_set_status($file, FILE_STATUS_PERMANENT);
  $file->timestamp = time();
  $form_state['values']['default_image'] = (array)$file;
 }
*/
/**
 * Implementation of CCK's hook_widget_settings($op = 'validate').
 */

function pluploadfield_widget_settings_validate($widget) {
  // Check that only web images are specified in the callback.
  $extensions = array_filter(explode(' ', $widget['file_extensions']));
  $web_extensions = unserialize(WEB_EXTENSIONS_ARRAY);
  if (count(array_diff($extensions, $web_extensions))) {
    form_set_error('file_extensions', t('Only web-standard images (jpg, gif, and png) and videos (mp4, ogv, webm, avi) are supported through the pluploadfield widget. If needing to upload other types of files, change the widget to use a standard file upload.'));
  }

  // Check that set resolutions are valid.
  foreach (array('min_resolution', 'max_resolution') as $resolution) {
    if (!empty($widget[$resolution]) && !preg_match('/^[0-9]+x[0-9]+$/', $widget[$resolution])) {
      form_set_error($resolution, t('Please specify a resolution in the format WIDTHxHEIGHT (e.g. 640x480).'));
    }
  }
}

/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */

function pluploadfield_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);
  return array_merge($filefield_settings, array('max_resolution', 'min_resolution', 'alt',  'custom_alt', 'title', 'custom_title', 'title_type', 'default_image', 'use_default_image'));
}

/**
 * Element #value_callback function.
 */
function pluploadfield_widget_value($element, $edit = FALSE) {
  $item = filefield_widget_value($element, $edit);
  if ($edit) {
    $item['alt'] = isset($edit['alt']) ? $edit['alt'] : '';
    $item['title'] = isset($edit['title']) ? $edit['title'] : '';
  }
  else {
    $item['alt'] = '';
    $item['title'] = '';
  }
  return $item;
}

/**
 * Element #process callback function.
 */
function pluploadfield_filefield_widget_process($element, $edit, &$form_state, $form) {
  $element = imagefield_widget_process($element, $edit, $form_state, $form);
  $element['#upload_validators']['pluploadfield_file_validate'] = array();
  return $element;
}

/**
 * FormAPI theme function. Theme the output of an image field.
 */
function theme_pluploadfield_widget($element) {
  drupal_add_css(drupal_get_path('module', 'pluploadfield') .'/pluploadfield.css');
  $element['#id'] .= '-upload'; // Link the label to the upload field.
  return theme('form_element', $element, $element['#children']);
}
